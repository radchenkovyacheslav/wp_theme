
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">

    <title><?php bloginfo('name'); ?> | <?php wp_title(); ?></title>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="Генератор продаж">
    <meta name="keywords" content="Генератор продаж, массовая рассылка в личные сообщения, рассылка сообщений, массовые рассылки, рекламное сообщение" >

    <link rel="apple-touch-icon" sizes="57x57" href="<?=get_template_directory_uri()?>/img/apple-icon-57x57.png?v=2">
    <link rel="apple-touch-icon" sizes="60x60" href="<?=get_template_directory_uri()?>/img/apple-icon-60x60.png?v=2">
    <link rel="apple-touch-icon" sizes="72x72" href="<?=get_template_directory_uri()?>/img/apple-icon-72x72.png?v=2">
    <link rel="apple-touch-icon" sizes="76x76" href="<?=get_template_directory_uri()?>/img/apple-icon-76x76.png?v=2">
    <link rel="apple-touch-icon" sizes="114x114" href="<?=get_template_directory_uri()?>/img/apple-icon-114x114.png?v=2">
    <link rel="apple-touch-icon" sizes="120x120" href="<?=get_template_directory_uri()?>/img/apple-icon-120x120.png?v=2">
    <link rel="apple-touch-icon" sizes="144x144" href="<?=get_template_directory_uri()?>/img/apple-icon-144x144.png?v=2">
    <link rel="apple-touch-icon" sizes="152x152" href="<?=get_template_directory_uri()?>/img/apple-icon-152x152.png?v=2">
    <link rel="apple-touch-icon" sizes="180x180" href="<?=get_template_directory_uri()?>/img/apple-icon-180x180.png?v=2">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?=get_template_directory_uri()?>/img/android-icon-192x192.png?v=2">
    <link rel="icon" type="image/png" sizes="32x32" href="<?=get_template_directory_uri()?>/img/favicon-32x32.png?v=2">
    <link rel="icon" type="image/png" sizes="96x96" href="<?=get_template_directory_uri()?>/img/favicon-96x96.png?v=2">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=get_template_directory_uri()?>/img/favicon-16x16.png?v=2">
    <link rel="manifest" href="<?=get_template_directory_uri()?>/img/manifest.json?v=2">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?=get_template_directory_uri()?>/img/ms-icon-144x144.png?v=2">
    <meta name="theme-color" content="#ffffff">

    <? wp_head(); ?>

</head>
<body>
<header class="header jumbotron">
    <div class="container">
        <div class="row">
            <div class="header_top clearfix">
                <div class="top_lef "><? the_field("slogan","theme_options")?></div>
                <div class="top_right ">
                    <ul>
                        <li>Звоните нам по номерам</li>
                        <li><a href="tel:+79852576393">+7(985) 257 63 93</a></li>
                        <li><a href="tel:+74994042688">+7(499) 404 26 88</a></li>
                        <li class="last">
                            <div class="wrap_a">
                                <a class="call_back test" href="#fanc">Обратный звонок</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row">
            <h1>Тысячи новых клиентов за копейки</h1>
            <div class="header_text">Массовая рассылка в <span>личные сообщения</span> <br> на популярных досках объявления</div>
            <div class="wrap_more wow zoomIn">
                <a class="learn_more" href="#fanc">Узнать больше</a></div>
        </div>
        <div class="row">
            <div class="header_bottom">
                <ul>
                    <li><div class="wrap_img"><img src="<?=get_template_directory_uri()?>/img/icon_coin.png" alt="coin"></div><span>Цена личного <br> сообщения</span></li>
                    <li><div class="wrap_img"><img src="<?=get_template_directory_uri()?>/img/icon_envelop.png" alt="envelop"></div><span>До миллиона сообщений<br> в сутки</span></li>
                    <li><div class="wrap_img"><img src="<?=get_template_directory_uri()?>/img/icon_target.png" alt="target"></div><span>Таргетинг по городам<br> и бизнес нишам</span></li>
                    <li><div class="wrap_img"><img src="<?=get_template_directory_uri()?>/img/icon_internet.png" alt="internet"></div><span>Ссылка на сайт и телефон <br>в каждом сообщении</span></li>
                </ul>
            </div>
        </div>
    </div>
</header>