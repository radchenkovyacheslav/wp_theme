<?php
get_header();

if (have_posts()) {
    while (have_posts()) {
        the_post();

        ?>
        <section class="playground">
            <div class="container">
<!--      =================================================          -->
                <?php
                $options = get_option("mytheme_settings");
                if($options['mytheme_checkbox_field_2'] == true) {
                    ?>

                    <h4><?php
                        the_field("title");
                        ?></h4>
                    <div class="row">
                        <ul class="ground">
                            <?php
                            if (have_rows("partners")) {
                                while (have_rows("partners")) {
                                    the_row();
                                    $img = get_sub_field("picture");
                                    ?>
                                    <li>
                                        <div class="ground_img"><img
                                                    src="<?= $img['url'] ?>"
                                                    alt="<?= $img['alt'] ?>"></div>
                                        <span><? the_sub_field("text") ?></span></li>
                                    <?

                                }
                            }
                            ?>


                        </ul>
                    </div>
                    <?php
                }
                    ?>
<!--               =========================================================== -->
                <div class="row country">
                    <h4>Наша программа ищет клиентов в <span>8 странах</span>:</h4>
                    <ul>
                        <li><img src="<?= get_template_directory_uri() ?>/img/pic_Russia.png"
                                 alt="Russia"><span>Россия</span></li>
                        <li><img src="<?= get_template_directory_uri() ?>/img/pic_Ukraine.png" alt="Ukraine"><span>Украина</span>
                        </li>
                        <li><img src="<?= get_template_directory_uri() ?>/img/pic_Poland.png"
                                 alt="Poland"><span>Польша</span></li>
                        <li><img src="<?= get_template_directory_uri() ?>/img/pic_Rumuniya.png" alt="Rumuniya"><span>Румыния</span>
                        </li>
                        <li><img src="<?= get_template_directory_uri() ?>/img/pic_Bolgarya.png" alt="Bolgarya"><span>Болгария</span>
                        </li>
                        <li><img src="<?= get_template_directory_uri() ?>/img/pic_Kazahstan.png" alt="Kazahstan"><span>Казахстан</span>
                        </li>
                        <li><img src="<?= get_template_directory_uri() ?>/img/pic_Belarus.png" alt="Belarus"><span>Беларусь</span>
                        </li>
                        <li><img src="<?= get_template_directory_uri() ?>/img/pic_uae.png" alt="uae"><span>UAE</span>
                        </li>
                    </ul>
                </div>
                <p>*Рассылка нашей программы по законодательству всех перечисленных стран не является спамом.</p>
            </div>
        </section>

        <section class="why jumbotron jumbotron_why">
            <div class="container">
                <h2 class="white">Почему массовые рассылки - оптимальный вариант?</h2>
                <div class="line"></div>
                <ul>
                    <li><img src="<?= get_template_directory_uri() ?>/img/thumb_down.png" alt="thumb_down"><span>Стоимость клика в ЯндексДирект<br> во многих нишах больше 500 <br>рублей</span>
                    </li>
                    <li><img src="<?= get_template_directory_uri() ?>/img/thumb_down.png" alt="thumb_down"><span>Стоимость клика в социальных<br> сетях - от 50 до 200 рублей.</span>
                    </li>
                    <li><img class="wow zoomIn " data-wow-duration="1.5s"
                             src="<?= get_template_directory_uri() ?>/img/thumd_up.png" alt="thumd_up"><span>В нашей программе стоимость<br> контакта с клиентом всего <br>
1 копейка - это в тысячи раз <br>выгоднее!</span></li>
                </ul>
                <h4>На данный момент у программы нет аналогов - это отличная<br>
                    возможность опередить конкурентов!</h4>
            </div>
        </section>

        <section class="service">
            <div class="container">
                <h2 class="black">Этот сервис подходит для:</h2>
                <div class="line"></div>
                <div class="row">
                    <ul>
                        <li><img src="<?= get_template_directory_uri() ?>/img/pic_1.png" alt="foto"><span>Предпринимателей</span>
                        </li>
                        <li><img src="<?= get_template_directory_uri() ?>/img/pic_2.png"
                                 alt="foto"><span>Маркетологов</span></li>
                        <li><img src="<?= get_template_directory_uri() ?>/img/pic_3.png" alt="foto"><span>Инфобизнесменов</span>
                        </li>
                        <li><img src="<?= get_template_directory_uri() ?>/img/pic_4.png" alt="foto"><span>Организаторов <br>мероприятий</span>
                        </li>
                        <li><img src="<?= get_template_directory_uri() ?>/img/pic_5.png" alt="foto"><span>MLM и FMCG<br> компаний</span>
                        </li>
                        <li><img src="<?= get_template_directory_uri() ?>/img/pic_6.png"
                                 alt="foto"><span>Служб такси</span></li>
                    </ul>
                </div>
                <p>Вообщем для всех, кому надо что-то продать...</p>
                <div class="wrap_find wow zoomIn">
                    <a class="more test" href="#fanc">Узнать подробнее</a></div>
            </div>
        </section>

        <section class="target jumbotron jumbotron_target">
            <div class="container">
                <h2 class="white">точная целевая аудитория:</h2>
                <p>Каждый человек с средним или высоким уровнем дохода создаёт<br>
                    около 30 объявлений в течении жизни. Личные сообщения,<br>
                    в отличие от объявлений, более точный и целевой канал рекламы.</p>
            </div>
        </section>

        <section class="how">
            <div class="container">
                <h2 class="black">как работает наш сервис</h2>
                <div class="line"></div>
                <div class="row">
                    <ul>
                        <li><img src="<?= get_template_directory_uri() ?>/img/pic_10.png" alt="icon"><span>Вы выбираете Вашу<br>  целевую аудиторию</span>
                        </li>
                        <li><img src="<?= get_template_directory_uri() ?>/img/pic_11.png" alt="icon"><span>Пишите рекламное <br>сообщение</span>
                        </li>
                        <li><img src="<?= get_template_directory_uri() ?>/img/pic_12.png" alt="icon"><span>Добавляете телефоны <br>и адрес сайта</span>
                        </li>
                        <li><img src="<?= get_template_directory_uri() ?>/img/pic_13.png" alt="icon"><span>Запускаете рассылку.<br>Всего 5 минут и Вы получили<br>поток клиентов!</span>
                        </li>
                    </ul>
                </div>
                <p class="need">Вам теперь не нужно тратить недели и месяцы на настройку контекстной<br> рекламы и
                    таргетинга в социальных сетях!</p>
                <p class="spam">*Рассылка нашей программы по законодательству всех перечисленных стран не является
                    спамом.</p>
            </div>
        </section>

        <section class="capabilities">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h2 class="black">Возможности программы:</h2>
                        <div class="line_cap"></div>
                        <ul>
                            <li>Неограниченная рассылка до 1млн. сообщений в день</li>
                            <li>Добавление ссылки Вашего сайта и номера телефона
                                в отправляемом письме
                            </li>
                            <li>Парсинг нужных контактов из досок объявлений</li>
                            <li>Таргетирование по городам, рубрикам, бизнес нишам</li>
                            <li>Возможность удалённого управления программой</li>
                            <li>Уникализация каждого отправляемого сообщения</li>
                            <li>Поддержка Прокси-серверов, сервисов Рукапча и Антигейт</li>
                            <li>СМС и Email рассылки потенциальным клиентам</li>
                        </ul>

                    </div>
                    <div class="col-md-6">
                        <img class="img-responsive" src="<?= get_template_directory_uri() ?>/img/generator.png"
                             alt="notebook">
                    </div>
                </div>
            </div>
        </section>

        <section class="cost">
            <div class="container">
                <h2 class="black">Стоимость пользования программой:</h2>
                <div class="line"></div>
                <div class="row">
                    <ul>
                        <li>
                            <div class="cost_wrap">
                                <div class="number_wrap">
                                    <p>1 месяц</p>
                                    <p class="number">9990</p>
                                    <p>рублей</p>
                                </div>
                                <div class="a_wrap">
                                    <a class="test" href="#fanc">Заказать</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="cost_wrap">
                                <div class="number_wrap">
                                    <p>1 месяц</p>
                                    <p class="cross">59400</p>
                                    <p class="number">24900</p>
                                    <p>рублей</p>
                                </div>
                                <div class="a_wrap">
                                    <a class="test" href="#fanc">Заказать</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="cost_wrap">
                                <div class="number_wrap">
                                    <p>1 месяц</p>
                                    <p class="cross">118000</p>
                                    <p class="number">39900</p>
                                    <p>рублей</p>
                                </div>
                                <div class="a_wrap">
                                    <a class="test" href="#fanc">Заказать</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <h2 class="black">хотите сделаем рассылку за вас (под ключ)?</h2>
                <div class="line"></div>
                <div class="row">
                    <table>
                        <thead class="key">
                        <tr>
                            <th class="td_title"><span>Колличество<br> сообщений</span></th>
                            <th class="td_number"><span>до<br> 100 000</span></th>
                            <th class="td_number"><span>от 100 000 <br>до 500 000</span></th>
                            <th class="td_number"><span>от 500 000 <br>до 1 000 000</span></th>
                            <th class="td_number"><span>от <br>1 000 000</span></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="td_title"><span>Цена за<br> 1 соообщение</span></td>
                            <td class="green"><span>9 коп</span></td>
                            <td class="green"><span>11 коп</span></td>
                            <td class="green"><span>7 коп</span></td>
                            <td class="green"><span>5 коп</span></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="wrap_order wow zoomIn">
                    <a class="test newsletter" href="#fanc">заказать рассылку</a></div>
                <div class="row cost_down">
                    <div class="cost_left">
                        <p><span>Хотите протестировать программу?</span><br>
                            Скачайте демо-версию бесплатно!<br>
                            (рассылка 500 сообщений)</p>
                    </div>
                    <div class="cost_right">
                        <a class="test free wow zoomIn" href="#fanc_free">Скачать бесплатно</a>
                    </div>
                </div>

            </div>
        </section>
        <?php

    }
}
get_footer();
?>