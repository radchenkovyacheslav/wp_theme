<?php


if(!function_exists('mytheme_enqueue_script')){
    function mytheme_enqueue_script(){
        wp_enqueue_style("animate", get_template_directory_uri()."/css/animate.min.css");
        wp_enqueue_style("bootstrap", get_template_directory_uri()."/css/bootstrap-grid.min.css");
        wp_enqueue_style("jquery.fancybox", get_template_directory_uri()."/css/jquery.fancybox.css");
        wp_enqueue_style("magnific-popup", get_template_directory_uri()."/css/magnific-popup.css");
        wp_enqueue_style("main", get_template_directory_uri()."/css/main.min.css");

        wp_enqueue_script("html5shiv", "https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js");
        wp_enqueue_script("respond", "https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js");
        wp_enqueue_script("html5shim", "http://html5shim.googlecode.com/svn/trunk/html5.js");

        wp_script_add_data( 'html5shiv', 'conditional', 'lt IE 9' );
        wp_script_add_data( 'respond', 'conditional', 'lt IE 9' );
        wp_script_add_data( 'html5shim', 'conditional', 'lt IE 9' );

        wp_deregister_script("jquery");
        wp_register_script("jquery","https://code.jquery.com/jquery-3.0.0.min.js");
        wp_enqueue_script("jquery");
        wp_enqueue_script("wow",get_template_directory_uri()."/js/wow.min.js",[],"",true);
        wp_enqueue_script("fancybox",get_template_directory_uri()."/js/jquery.fancybox.pack.js",["jquery"],"",true);
        wp_enqueue_script("script",get_template_directory_uri()."/js/script.min.js",["jquery"],"",true);



        /*
                wp_enqueue_script("html5shiv", "https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js");
                wp_enqueue_script("html5shiv", "https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js");
           */ }
}

add_action("wp_enqueue_scripts","mytheme_enqueue_script");


if( function_exists('acf_add_options_page') ) {

    $args = array(

        /* (string) The title displayed on the options page. Required. */
        'page_title' => __('Настройки темы'),

        /* (string) The title displayed in the wp-admin sidebar. Defaults to page_title */
        'menu_title' => __('theme options'),

        /* (string) The slug name to refer to this menu by (should be unique for this menu).
        Defaults to a url friendly version of menu_slug */
        'menu_slug' => '',

        /* (string) The capability required for this menu to be displayed to the user. Defaults to edit_posts.
        Read more about capability here: http://codex.wordpress.org/Roles_and_Capabilities */
        'capability' => 'edit_posts',

        /* (int|string) The position in the menu order this menu should appear.
        WARNING: if two menu items use the same position attribute, one of the items may be overwritten so that only one item displays!
        Risk of conflict can be reduced by using decimal instead of integer values, e.g. '63.3' instead of 63 (must use quotes).
        Defaults to bottom of utility menu items */
        'position' => false,

        /* (string) The slug of another WP admin page. if set, this will become a child page. */
        'parent_slug' => '',

        /* (string) The icon class for this menu. Defaults to default WordPress gear.
        Read more about dashicons here: https://developer.wordpress.org/resource/dashicons/ */
        'icon_url' => false,

        /* (boolean) If set to true, this options page will redirect to the first child page (if a child page exists).
        If set to false, this parent page will appear alongside any child pages. Defaults to true */
        'redirect' => true,

        /* (int|string) The '$post_id' to save/load data to/from. Can be set to a numeric post ID (123), or a string ('user_2').
        Defaults to 'options'. Added in v5.2.7 */
        'post_id' => 'theme_options',

        /* (boolean)  Whether to load the option (values saved from this options page) when WordPress starts up.
        Defaults to false. Added in v5.2.8. */
        'autoload' => false,

    );
    acf_add_options_page( $args );


}




/*===========================================================*/

add_action( 'admin_menu', 'mytheme_add_admin_menu' );
add_action( 'admin_init', 'mytheme_settings_init' );


function mytheme_add_admin_menu(  ) {

    add_options_page( 'mytheme', 'mytheme', '', 'mytheme',
        'mytheme_options_page' );

    add_menu_page( "Страница опций", "Опции темы", "manage_options",
        "themeoptions", "mytheme_options_page", "none", 81 );
}


function mytheme_settings_init(  ) {

    register_setting( 'pluginPage', 'mytheme_settings' );

    add_settings_section(
        'mytheme_pluginPage_section',
        __( 'Your section description', 'mytheme' ),
        'mytheme_settings_section_callback',
        'pluginPage'
    );

    add_settings_field(
        'mytheme_radio_field_0',
        __( 'Settings field description', 'mytheme' ),
        'mytheme_radio_field_0_render',
        'pluginPage',
        'mytheme_pluginPage_section'
    );

    add_settings_field(
        'mytheme_radio_field_1',
        __( 'Settings field description', 'mytheme' ),
        'mytheme_radio_field_1_render',
        'pluginPage',
        'mytheme_pluginPage_section'
    );

    add_settings_field(
        'mytheme_checkbox_field_2',
        __( 'Show Dashbords', 'mytheme' ),
        'mytheme_checkbox_field_2_render',
        'pluginPage',
        'mytheme_pluginPage_section'
    );

    add_settings_field(
        'mytheme_checkbox_field_3',
        __( 'Settings field description', 'mytheme' ),
        'mytheme_checkbox_field_3_render',
        'pluginPage',
        'mytheme_pluginPage_section'
    );

    add_settings_field(
        'mytheme_text_field_4',
        __( 'Settings field description', 'mytheme' ),
        'mytheme_text_field_4_render',
        'pluginPage',
        'mytheme_pluginPage_section'
    );




}


function mytheme_radio_field_0_render(  ) {

    $options = get_option( 'mytheme_settings' );
    ?>
    <input type='radio' name='mytheme_settings[mytheme_radio_field_0]' <?php checked( $options['mytheme_radio_field_0'], 1 ); ?> value='1'>
    <?php

}


function mytheme_radio_field_1_render(  ) {

    $options = get_option( 'mytheme_settings' );
    ?>
    <input type='radio' name='mytheme_settings[mytheme_radio_field_1]' <?php checked( $options['mytheme_radio_field_1'], 1 ); ?> value='1'>
    <?php

}


function mytheme_checkbox_field_2_render(  ) {

    $options = get_option( 'mytheme_settings' );
    ?>
    <input type='checkbox' name='mytheme_settings[mytheme_checkbox_field_2]' <?php checked( $options['mytheme_checkbox_field_2'], 1 ); ?> value='1'>
    <?php

}


function mytheme_checkbox_field_3_render(  ) {

    $options = get_option( 'mytheme_settings' );
    ?>
    <input type='checkbox' name='mytheme_settings[mytheme_checkbox_field_3]' <?php checked( $options['mytheme_checkbox_field_3'], 1 ); ?> value='1'>
    <?php

}


function mytheme_text_field_4_render(  ) {

    $options = get_option( 'mytheme_settings' );
    ?>
    <input type='text' name='mytheme_settings[mytheme_text_field_4]' value='<?php echo $options['mytheme_text_field_4']; ?>'>
    <?php

}


function mytheme_settings_section_callback(  ) {

    echo __( 'This section description', 'mytheme' );

}


function mytheme_options_page(  ) {

    ?>
    <form action='options.php' method='post'>

        <h2>mytheme</h2>

        <?php
        settings_fields( 'pluginPage' );
        do_settings_sections( 'pluginPage' );
        submit_button();
        ?>

    </form>
    <?php

}