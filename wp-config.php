<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_theme');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '123');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '.@-[[r|U<zkCU@-qQSvjypzz)9@YFZlleu|)5UxrPkM&,U1!M,DzY.Oy/2> 3qA>');
define('SECURE_AUTH_KEY',  'b^*1Cesr>d_<YH9}ujGNN&#uV/L!e[nBfxWE^`=F$:KLH?aXmVf!=v$BLY7hTB!S');
define('LOGGED_IN_KEY',    '@[[wIv~.6TocQDNW^np?+@LL+X=r>(l?3PG3i; *UA5Y3o$fX~u22kx&#B+a(zmk');
define('NONCE_KEY',        ' Ac19p3sJr9%7AHMc~dfE/7xOVnwHQfI6qLE?&pR7weMr gd#5tr0+[(Wq6nTwXG');
define('AUTH_SALT',        '7{:/*Z;CEL(D:6Xc4onbEo5.^Gf|E0#@6,!;_w};%MhWIrdf3(HJM;fK@t}0A]JF');
define('SECURE_AUTH_SALT', 'nnyX?b+D9d,JA(Q26<5D`e~%3BYFlADIk)EirzD|)TRP7+Y6xd%_E9Yc&RC>9[r/');
define('LOGGED_IN_SALT',   'v!h_JP&|QU,A@(@:>8u@#9ha<c{6V.q7;R[yAryphBD&.PL) W<~(6/`I![LQUBG');
define('NONCE_SALT',       'KBk9<n>XzZ&j}C?_lx<2Gt{dF3rK(8iw<FJ_0,{. sjH>=wW1i{m49ShW9.Bqm23');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
